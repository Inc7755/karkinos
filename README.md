https://cdn.darknet.org.uk/wp-content/uploads/2021/08/Karkinos-Beginner-Friendly-Penetration-Testing-Tool-640x425.png

Karkinos - Beginner Friendly Penetration Testing Tool

Karkinos Beginner Friendly Penetration Testing Tool Features
Encoding/Decoding characters
Encrypting/Decrypting text or files
Reverse shell handling
Cracking and generating hashes
How to Install Karkinos Beginner Friendly Penetration Testing Tool
Dependencies are:

Any server capable of hosting PHP
Tested with PHP 7.4.9
Tested with Python 3.8
Make sure it is in your path as:
Windows: python
Linux: python3
If it is not, please change the commands in includes/pid.php
Pip3
Raspberry Pi Zero friendly :) (crack hashes at your own risk)
Then:

git clone https://github.com/helich0pper/Karkinos.git
cd Karkinos
pip3 install -r requirements.txt
cd wordlists && unzip passlist.zip You can also unzip it manually using file explorer. Just make sure passlist.txt is in wordlists directory.
Make sure you have write privilages for db/main.db
Enable extension=mysqli in your php.ini file.
If you don’t know where to find this, refer to the PHP docs. Note: MySQLi is only used to store statistics.
Thats it! Now just host it using your preferred web server or run: php -S 127.0.0.1:8888 in the Karkinos directory.
Important: using port 5555, 5556, or 5557 will conflict with the Modules
If you insist on using these ports, change the PORT value in:

/bin/Server/app.py Line 87
/bin/Busting/app.py Line 155
/bin/PortScan/app.py Line 128